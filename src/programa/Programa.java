package programa;

import clases.Colegio;

public class Programa {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		/**
		 * 
		 */
		int maxAlumnos=7;
		Colegio colegio1= new Colegio(maxAlumnos);
		
		/**
		 * Doy de alta a los alumnos cogiendo el metodo altaAlumno de la clase colegio
		 */
		System.out.println("Dar de alta alumno");
		System.out.println("Doy de alta a Pepe,Marta,Ines,Lionel,Neymar,Jaime y Danara");
		colegio1.altaAlumno("Pepe", "Smith", "2000-07-22", 7, "XSFA", "Primero");
		colegio1.altaAlumno("Ines", "Gimenez", "2000-04-08", 2, "XDPO", "Primero");
		colegio1.altaAlumno("Lionel", "Messi", "2001-10-11", 10, "LOLS", "Primero");
		colegio1.altaAlumno("Neymar", "Junior", "1999-02-05", 3, "POSL", "Segundo");
		colegio1.altaAlumno("Jaime", "Reyes", "1998-12-09", 5, "MBAD", "Segundo");
		colegio1.altaAlumno("Marta", "Perez", "1999-02-08", 6, "FFAR", "Segundo");
		colegio1.altaAlumno("Danara", "Exposito", "1995-05-15", 1, "KLTY", "Tercero");
		
		/**
		 * Muestro a todos los alumnos cogiendo el metodo listarAlumnos de la clase colegio
		 */
		System.out.println("Listar alumnos");
		colegio1.listarAlumnos();
		
		/**
		 * Busco a un alumno por su matricula y lo muestro por pantalla
		 */
		System.out.println("Buscar alumno");
		System.out.println("Busco alumno Marta");
		System.out.println(colegio1.buscarAlumnos("FFAR"));
		
		/**
		 * Elimino a un alumno por su matricula y lo muestro por pantalla
		 * Utilizando el metodo eliminarAlumnos de la clase colegio
		 */
		System.out.println("Eliminar alumno");
		System.out.println("Elimino alumno Pepe");
		colegio1.eliminarAlumnos("XSFA");
		colegio1.listarAlumnos();
		
		
		System.out.println("Dar de alta nuevo alumno");
		System.out.println("Doy de alta a Neymar");
		colegio1.altaAlumno("Neymar", "Santos", "1995-03-10",6, "POSL", "Cuarto");
		colegio1.listarAlumnos();
		
		/**
		 * Uso el metodo cambiarAlumnos de la clase colegio para modificar la nota
		 */
		System.out.println("Cambio la nota de un alumno");
		System.out.println("Cambio la nota de Ines de un 2 a un 9");
		colegio1.cambiarAlumnos("XDPO", 9);
		colegio1.listarAlumnos();
		
		/**
		 * Uso el metodo listarAlumnoPorCurso de la clase colegio para listar por curso
		 */
		System.out.println("Listar alumno por curso");
		System.out.println("listo los alumnos del primer curso");
		colegio1.listarAlumnoPorCurso("Primero");
		
		
	}

}
