package clases;

import java.time.LocalDate;

/**
 * Esta clase tendra, un constructor, atributo con array y metodos
 * @author Anthony Araujo
 *
 */
public class Colegio {
	
	/**
	 * Creo un Atributo array de la clase alumno llamada alumnos
	 */
	private Alumno[] alumnos;
	
	/**
	 * Constructor
	 * @param maxAlumnos inicializa el array alumnos
	 */
	public Colegio(int maxAlumnos) {
		this.alumnos= new Alumno[maxAlumnos];
	}
	
	/**
	 * Metodo altaAlumno crea una instacia de alumno dandole valor a sus atributos
	 * y la a�ade a la 1� posicion libre del array
	 */
	public void altaAlumno(String nombre, String apellido,String fechaNacimiento,int nota, 
			String matricula, String curso) {
		for(int i=0;i<alumnos.length;i++) {
			if(alumnos[i]==null) {
				alumnos[i]= new Alumno(matricula);
				alumnos[i].setNombre(nombre);
				alumnos[i].setApellido(apellido);
				alumnos[i].setCurso(curso);
				alumnos[i].setNota(nota);
				alumnos[i].setFechaNacimiento(LocalDate.parse(fechaNacimiento));
				break;
			}
		}
	}
	
	/**
	 * Metodo listarAlumnos
	 * Muestra por consola todos los datos de los alumnos que tengo en el array
	 */
	public void listarAlumnos(){
		for(int i=0;i<alumnos.length;i++) {
			if(alumnos[i]!=null) {
				System.out.println(alumnos[i]);
			}
		}
	}
	
	/**
	 * Metodo buscarAlumnos
	 * @param matricula 
	 * @return devuelve el objeto alumno si coincide con el parametro matricula
	 */
	public Alumno buscarAlumnos(String matricula){
		for(int i=0;i<alumnos.length;i++) {
			if(alumnos[i]!=null) {
				if(alumnos[i].getMatricula().equals(matricula)) {
					return alumnos[i];
				}
			}
		}
		return null;
	}
	
	/**
	 * Metodo eliminarAlumnos 
	 * @param matricula busca en el array, el alumno que coincide con el parametro
	 * recibido y eliminarlo de esa posicion del array
	 * Si no existe el alumno con dicha matricula no hara nada
	 */
	public void eliminarAlumnos(String matricula){
		for(int i=0;i<alumnos.length;i++) {
			if(alumnos[i]!=null) {
				if(alumnos[i].getMatricula().equals(matricula)) {
					alumnos[i]=null;
				}
			}
		}
	}
	
	/**
	 * metodo cambiarAlumnos
	 * @param matricula 
	 * @param nota1 
	 */
	public void cambiarAlumnos(String matricula,int nota1) {
		for(int i=0;i<alumnos.length;i++) {
			if(alumnos[i]!=null) {
				if(alumnos[i].getMatricula().equals(matricula)) {
					alumnos[i].setNota(nota1);
				}
			}
		}
	}
	
	/**
	 * metodo listarAlumnoPorCurso
	 * @param curso 
	 */
	
	public void listarAlumnoPorCurso(String curso) {
		for(int i=0;i<alumnos.length;i++) {
			if(alumnos[i]!=null) {
				if(alumnos[i].getCurso().equals(curso)) {
					System.out.println(alumnos[i]);
				}
			}
		}
	}

	
}
