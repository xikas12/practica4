package clases;

import java.time.LocalDate;

/**
 * En esta clase alumno llevara un atributos, metodo, un unico constructor, setter and getter 
 * @author Anthony Araujo
 *
 */
public class Alumno {
	
	/**
	 * Aqui defino los atributos que va a tener la clase alumno
	 * 
	 */
	private String nombre;
	private String apellido;
	private LocalDate fechaNacimiento;
	private int nota;
	private String matricula;
	private String curso;
	
	/**
	 * Constructor de la clase alumnos
	 * @param matricula recibe el string matricula e iniciliza ese atributo
	 */
	public Alumno (String matricula){
		this.matricula = matricula;
	}
	
	/**
	 * Setter and getter, cambia o modifica a los alumnos
	 * @return devuelve los atributos
	 */

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public LocalDate getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(LocalDate fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public int getNota() {
		return nota;
	}

	public void setNota(int nota) {
		this.nota = nota;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}
	
	/**
	 * Metodo toString
	 * @return devuelve un string con sus datos
	 */
	@Override
	public String toString() {
		return "Alumno [nombre= " + nombre + ", apellido= " + apellido + ", fechaNacimiento= " + fechaNacimiento
				+ ", nota= " + nota + ", matricula= " + matricula + ", curso= " + curso + "]";
	}
	
	
	
}
